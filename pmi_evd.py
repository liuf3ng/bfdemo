#!/usr/bin/python3
import numpy as np
from functools import reduce
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import matplotlib.cm as cm
np.set_printoptions(precision = 6, suppress = True)

wavelength = 2

bs_cord = (0,0)
bs_ant_num = 8
bs_ant_interval = 1
bs_ant = 1j * np.linspace(bs_cord[1] - bs_ant_interval * (bs_ant_num - 1) / 2,
        bs_cord[1] + bs_ant_interval * (bs_ant_num - 1) / 2, bs_ant_num) + bs_cord[0]

ue_ant_num = 4
ue_ant_interval = 1
ue_cord0 = (800 ,300)
ue_cord1 = (800 ,0)

ue_ant0 = 1j * np.linspace(ue_cord0[1] - ue_ant_interval * (ue_ant_num - 1) / 2,
        ue_cord0[1] + ue_ant_interval * (ue_ant_num - 1) / 2, ue_ant_num) + ue_cord0[0]

ue_ant1 = 1j * np.linspace(ue_cord1[1] - ue_ant_interval * (ue_ant_num - 1) / 2,
        ue_cord1[1] + ue_ant_interval * (ue_ant_num - 1) / 2, ue_ant_num) + ue_cord1[0]

def type1Pmi(i11, i12,i2, N1, N2, O1, O2):
    Pcsirs = 2 * N1 * N2
    l = i11
    m = i12
    n = i2
    um = np.exp(1j * 2 * np.pi * m / O2 / N2 * np.array(range(0,N2)))
    vl = np.exp(1j * 2 * np.pi * l / O1 / N1 * np.array(range(0,N1)))
    vlm = np.kron(vl, um)
    phin = np.exp(1j * np.pi * n / 2)
    return 1 / np.sqrt(Pcsirs) * (np.append(vlm , [phin * vlm]))

def getH(bs_ant, ue_ant):
    xx, yy = np.meshgrid(ue_ant, bs_ant)
    dis = np.absolute(xx - yy)
    H = np.exp(-1j * 2 * np.pi * dis / wavelength)
    return H

def getMag(xx, yy, bs_ant, weight):
    zz = xx + 1j * yy
    zz = np.absolute(reduce(lambda x, y:x + y, map(lambda x, y:np.exp(-1j * 2 *
        np.pi * np.absolute(zz - x) / wavelength) * np.absolute(np.cos(np.angle(zz - x))) * y, bs_ant, weight)))
    return zz

def getSvdWeight(H):
    U,sigma,Vh = np.linalg.svd(H, full_matrices = False)
    sigma = np.diag(sigma)
    norm = np.linalg.norm(Vh, axis = -1)[:, np.newaxis]
    return np.conjugate(Vh / norm)

def getIndirectSvdWeight(H):
    HH = np.conjugate(np.transpose(H))
    Rhh = np.matmul(H, HH)
    eigenValue, eigenVec = np.linalg.eig(Rhh)
    eigenVec = np.conjugate(np.transpose(eigenVec))
    Vh = np.matmul(eigenVec, H)
    norm = np.linalg.norm(Vh, axis = -1)[:, np.newaxis]
    return np.conjugate(Vh / norm)

def getEvdWeight(H):
    HH = np.conjugate(np.transpose(H))
    Rhh = np.matmul(HH, H)
    eigenValue, eigenVec = np.linalg.eig(Rhh)
    eigenVec = np.conjugate(np.transpose(eigenVec))
    norm = np.linalg.norm(eigenVec, axis = -1)[:, np.newaxis]
    return np.conjugate(eigenVec / norm)

x = np.linspace(0,1000,1000)
y = np.linspace(-400,600,1000)
xx, yy = np.meshgrid(x, y)
fig = plt.figure()
fig.set_figwidth(20)
fig.set_figheight(15)
H = getH(ue_ant0, bs_ant)
svdWeight = getSvdWeight(H)
#evdWeight = getEvdWeight(H)
#indirectEvd = getIndirectSvdWeight(H)
#for layer in range(0,4):
    #plt.subplot(3,4, layer + 1)
    #zz = getMag(xx, yy, bs_ant,svdWeight[layer])
    #plt.contourf(xx, yy, zz, levels = 50)
    #plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
    #plt.colorbar()
    #plt.subplot(3,4, layer + 5)
    #zz = getMag(xx, yy, bs_ant,evdWeight[layer])
    #plt.contourf(xx, yy, zz, levels = 50)
    #plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
    #plt.colorbar()
    #plt.subplot(3,4, layer + 9)
    #zz = getMag(xx, yy, bs_ant,indirectEvd[layer])
    #plt.contourf(xx, yy, zz, levels = 50)
    #plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
    #plt.colorbar()
#plt.savefig("/mnt/d/wave.png",dpi = 200)

stella = np.transpose((np.exp(-1j * np.pi * np.linspace(0, 2, 64, endpoint = False)) * np.sqrt(0.5))[:, np.newaxis])
color = cm.rainbow(np.linspace(0,1,stella.size))
weight = np.transpose(svdWeight[0][np.newaxis, :])
y = np.matmul(np.matmul(H, weight),stella)
fig = plt.figure()
fig.set_figwidth(15)
fig.set_figheight(15)
for ant in range(0,4):
    plt.subplot(2,2, ant + 1)
    plt.scatter(y[ant,:].real, y[ant,:].imag, color = color)
plt.savefig("/mnt/d/stella{}.png".format(0),dpi = 200)
N1 = 8
N2 = 1
O1 = 1
O2 = 1
index = 1
fig = plt.figure()
fig.set_figwidth(N2 * O2 * 5)
fig.set_figheight(N1 * O1 * 5)
for i11 in range(0, N1 * O1):
    for i12 in range(0, N2 * O2):
        pmiWeight = type1Pmi(i11,i12, 1 ,N1, N2 ,O1, O2)
        pmiWeight = pmiWeight[0:N1 * N2][:,np.newaxis]
        fusionweight = np.multiply(pmiWeight, weight)
        plt.subplot(N1 * O1, N2 * O2, index)
        zz = getMag(xx, yy, bs_ant,fusionweight)
        plt.contourf(xx, yy, zz, levels = 50)
        plt.colorbar()
        index += 1
plt.savefig("/mnt/d/pmi.png")
