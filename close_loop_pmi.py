#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from functools import reduce
import matplotlib
matplotlib.use("Agg")
import matplotlib.cm as cm
np.set_printoptions(precision = 6)

wavelength = 2

bs_cord = (0,0)
bs_ant_num = 8
bs_ant_interval = 1
bs_ant = 1j * np.linspace(bs_cord[1] - bs_ant_interval * (bs_ant_num - 1) / 2,
        bs_cord[1] + bs_ant_interval * (bs_ant_num - 1) / 2, bs_ant_num) + bs_cord[0]
bs_ant[bs_ant.size // 2:] = bs_ant[0:bs_ant.size // 2]

ue_ant_num = 4
ue_ant_interval = 1
ue_cord0 = (800,0)

ue_ant0 = 1j * np.linspace(ue_cord0[1] - ue_ant_interval * (ue_ant_num - 1) / 2,
        ue_cord0[1] + ue_ant_interval * (ue_ant_num - 1) / 2, ue_ant_num) + ue_cord0[0]
ue_ant0[ue_ant0.size // 2:] = ue_ant0[0:ue_ant0.size // 2]

def getH(bs_ant, ue_ant):
    xx, yy = np.meshgrid(ue_ant, bs_ant)
    dis = np.absolute(xx - yy)
    H = np.exp(-1j * 2 * np.pi * dis / wavelength) / dis / dis
    return H

def getMag(xx, yy, bs_ant, weight):
    zz = xx + 1j * yy
    zz = np.absolute(reduce(lambda x, y:x + y, map(lambda x, y:np.exp(-1j * 2 *
        np.pi * np.absolute(zz - x) / wavelength) * y, bs_ant, weight)))
    return zz

def getSvdWeight(H):
    U,sigma,Vh = np.linalg.svd(H, full_matrices = False)
    sigma = np.diag(sigma)
    norm = np.linalg.norm(Vh, axis = -1)[:, np.newaxis]
    return np.conjugate(Vh / norm)

def type1Pmi(i11, i12,i2, N1, N2, O1, O2):
    Pcsirs = 2 * N1 * N2
    l = i11
    m = i12
    n = i2
    um = np.exp(1j * 2 * np.pi * m / O2 / N2 * np.array(range(0,N2)))
    vl = np.exp(1j * 2 * np.pi * l / O1 / N1 * np.array(range(0,N1)))
    vlm = np.kron(vl, um)
    phin = np.exp(1j * np.pi * n / 2)
    return 1 / np.sqrt(Pcsirs) * (np.append(vlm , [phin * vlm]))

outterWeight = np.diag([1,1,1,1,1,1,1,1])

sigma = 1e-7
N1 = 4
N2 = 1
O1 = 4
O2 = 1
x = np.linspace(0,1000,1000)
y = np.linspace(-900,800,1000)
xx, yy = np.meshgrid(x, y)
fig = plt.figure()
fig.set_figwidth(N2 * O2 * 5)
fig.set_figheight(N1 * O1 * 5)
index = 1
for i11 in range(0, 4 * 4):
    for i12 in range(0,1):
            H = getH(ue_ant0, bs_ant)
            csirs_symbol = 1
            signal = csirs_symbol * outterWeight
            y = np.matmul(H, signal)
            H = y
            innerWeight = type1Pmi(i11,i12,0,N1, N2, O1, O2).reshape((8,1))
            Hri = np.matmul(H, innerWeight)
            HriH = np.transpose(np.conjugate(Hri))
            noise = np.identity(4) * sigma * sigma
            P = np.matmul(np.matmul(HriH, np.linalg.inv(np.matmul(Hri, HriH) + noise)), Hri)
            ptrace = np.absolute(np.trace(P))
            sinr = ptrace / (1 - ptrace)
            sinr = 10 * np.log10(sinr)
            print("sinr under i11 i12 is:", i11, i12, sinr)
            weight = np.matmul(outterWeight, innerWeight)[0:4]
            #print(weight)
            plt.subplot(N1 * O1, N2 * O2, index)
            zz = getMag(xx, yy, bs_ant,weight)
            plt.contourf(xx, yy, zz, levels = 50)
            plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
            plt.colorbar()
            index += 1
plt.savefig("/mnt/d/pmi.png")
