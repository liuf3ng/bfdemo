#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from functools import reduce
import matplotlib
matplotlib.use("Agg")

wavelength = 2

bs_cord = (0,0)
bs_ant_num = 8
bs_ant_interval = 1
bs_ant = 1j * np.linspace(bs_cord[1] - bs_ant_interval * (bs_ant_num - 1) / 2,
        bs_cord[1] + bs_ant_interval * (bs_ant_num - 1) / 2, bs_ant_num) + bs_cord[0]

ue_ant_num = 4
ue_ant_interval = 1
ue_cord0 = (800 ,300)
ue_cord1 = (800 ,0)

ue_ant0 = 1j * np.linspace(ue_cord0[1] - ue_ant_interval * (ue_ant_num - 1) / 2,
        ue_cord0[1] + ue_ant_interval * (ue_ant_num - 1) / 2, ue_ant_num) + ue_cord0[0]

ue_ant1 = 1j * np.linspace(ue_cord1[1] - ue_ant_interval * (ue_ant_num - 1) / 2,
        ue_cord1[1] + ue_ant_interval * (ue_ant_num - 1) / 2, ue_ant_num) + ue_cord1[0]

def getH(bs_ant, ue_ant):
    xx, yy = np.meshgrid(ue_ant, bs_ant)
    H = np.exp(-1j * 2 * np.pi * np.absolute(xx - yy) / wavelength)
    return H

def getMag(xx, yy, bs_ant, weight):
    zz = xx + 1j * yy
    zz = np.absolute(reduce(lambda x, y:x + y, map(lambda x, y:np.exp(1j * 2 *
        np.pi * np.absolute(x - zz) / wavelength) * y, bs_ant, weight)))
    return zz

def getSu(ue_ant,bs_ant):
    H = getH(ue_ant, bs_ant)
    s,v,d = np.linalg.svd(H)
    return d[0]

x = np.linspace(0,1000,1000)
y = np.linspace(-400,600,1000)
xx, yy = np.meshgrid(x, y)
weight0 = getSu(ue_ant0, bs_ant)
weight1 = getSu(ue_ant1, bs_ant)
plt.subplot(221)
zz = getMag(xx, yy, bs_ant,weight0)
plt.contourf(xx, yy, zz, levels = 50)
plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
plt.plot(ue_cord1[0],ue_cord1[1], 'ob')
plt.colorbar()
plt.subplot(222)
zz = getMag(xx, yy, bs_ant,weight1)
plt.contourf(xx, yy, zz, levels = 50)
plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
plt.plot(ue_cord1[0],ue_cord1[1], 'ob')
plt.colorbar()

su = np.concatenate([weight0, weight1]).reshape(2,bs_ant_num)
temp = np.matmul(np.matrix(su), np.matrix(su).getH())
temp = np.linalg.inv(temp)
mu = np.matmul(temp, np.matrix(su))
weight0 = mu[0].getA1()
weight1 = mu[1].getA1()
plt.subplot(223)
zz = getMag(xx, yy, bs_ant,weight0)
plt.contourf(xx, yy, zz, levels = 50)
plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
plt.plot(ue_cord1[0],ue_cord1[1], 'ob')
plt.colorbar()
plt.subplot(224)
zz = getMag(xx, yy, bs_ant,weight1)
plt.contourf(xx, yy, zz, levels = 50)
plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
plt.plot(ue_cord1[0],ue_cord1[1], 'ob')
plt.colorbar()
plt.savefig("/mnt/d/wave.png",dpi = 200)
