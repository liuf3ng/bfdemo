#!/usr/bin/python3
import numpy as np
import matplotlib.pyplot as plt
from functools import reduce
import matplotlib
matplotlib.use("Agg")
import matplotlib.cm as cm
np.set_printoptions(precision = 6, suppress = True)

wavelength = 2

bs_cord = (0,0)
bs_ant_num = 8
bs_ant_interval = 1
bs_ant = 1j * np.linspace(bs_cord[1] - bs_ant_interval * (bs_ant_num - 1) / 2,
        bs_cord[1] + bs_ant_interval * (bs_ant_num - 1) / 2, bs_ant_num) + bs_cord[0]

ue_ant_num = 4
ue_ant_interval = 1
ue_cord0 = (800 ,300)
ue_cord1 = (800 ,0)

ue_ant0 = 1j * np.linspace(ue_cord0[1] - ue_ant_interval * (ue_ant_num - 1) / 2,
        ue_cord0[1] + ue_ant_interval * (ue_ant_num - 1) / 2, ue_ant_num) + ue_cord0[0]

ue_ant1 = 1j * np.linspace(ue_cord1[1] - ue_ant_interval * (ue_ant_num - 1) / 2,
        ue_cord1[1] + ue_ant_interval * (ue_ant_num - 1) / 2, ue_ant_num) + ue_cord1[0]

def getH(bs_ant, ue_ant):
    xx, yy = np.meshgrid(ue_ant, bs_ant)
    dis = np.absolute(xx - yy)
    H = np.exp(-1j * 2 * np.pi * dis / wavelength) / dis / dis
    return H

def getMag(xx, yy, bs_ant, weight):
    zz = xx + 1j * yy
    zz = np.absolute(reduce(lambda x, y:x + y, map(lambda x, y:np.exp(-1j * 2 *
        np.pi * np.absolute(zz - x) / wavelength) * np.absolute(np.cos(np.angle(zz - x))) * y, bs_ant, weight)))
    return zz

def getSvdWeight(H):
    U,sigma,Vh = np.linalg.svd(H, full_matrices = False)
    sigma = np.diag(sigma)
    norm = np.linalg.norm(Vh, axis = -1)[:, np.newaxis]
    return np.conjugate(Vh / norm)

def getIndirectSvdWeight(H):
    HH = np.conjugate(np.transpose(H))
    Rhh = np.matmul(H, HH)
    eigenValue, eigenVec = np.linalg.eig(Rhh)
    eigenVec = np.conjugate(np.transpose(eigenVec))
    Vh = np.matmul(eigenVec, H)
    norm = np.linalg.norm(Vh, axis = -1)[:, np.newaxis]
    return np.conjugate(Vh / norm)

def getEvdWeight(H):
    HH = np.conjugate(np.transpose(H))
    Rhh = np.matmul(HH, H)
    eigenValue, eigenVec = np.linalg.eig(Rhh)
    eigenVec = np.conjugate(np.transpose(eigenVec))
    norm = np.linalg.norm(eigenVec, axis = -1)[:, np.newaxis]
    return np.conjugate(eigenVec / norm)

x = np.linspace(0,1000,1000)
y = np.linspace(-400,600,1000)
xx, yy = np.meshgrid(x, y)
fig = plt.figure()
fig.set_figwidth(20)
fig.set_figheight(15)
H = getH(ue_ant0, bs_ant)
svdWeight = getSvdWeight(H)
evdWeight = getEvdWeight(H)
indirectEvd = getIndirectSvdWeight(H)
for i in range(0,4):
    plt.subplot(3,4, i + 1)
    zz = getMag(xx, yy, bs_ant,svdWeight[i])
    plt.contourf(xx, yy, zz, levels = 50)
    plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
    plt.colorbar()
    plt.subplot(3,4, i + 5)
    zz = getMag(xx, yy, bs_ant,evdWeight[i])
    plt.contourf(xx, yy, zz, levels = 50)
    plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
    plt.colorbar()
    plt.subplot(3,4, i + 9)
    zz = getMag(xx, yy, bs_ant,indirectEvd[i])
    plt.contourf(xx, yy, zz, levels = 50)
    plt.plot(ue_cord0[0],ue_cord0[1], 'ob')
    plt.colorbar()
plt.savefig("/mnt/d/wave.png",dpi = 200)

stella = np.transpose((np.exp(-1j * np.pi * np.linspace(0, 2, 64, endpoint = False)) * np.sqrt(0.5))[:, np.newaxis])
color = cm.rainbow(np.linspace(0,1,stella.size))
for layer in range(0,4):
    weight = np.transpose(svdWeight[layer][np.newaxis, :])
    y = np.matmul(np.matmul(H, weight),stella)
    fig = plt.figure()
    fig.set_figwidth(15)
    fig.set_figheight(15)
    for ant in range(0,4):
        plt.subplot(2,2, ant + 1)
        plt.scatter(y[ant,:].real, y[ant,:].imag, color = color)
    plt.savefig("/mnt/d/stella{}.png".format(layer),dpi = 200)
