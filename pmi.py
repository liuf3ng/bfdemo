#!/usr/bin/python3
import numpy as np
np.set_printoptions(formatter = {'complexfloat': '({0.real: 0.3f}, {0.imag: 0.3f}i)'.format})
import matplotlib
import matplotlib.pyplot as plt
matplotlib.use("Agg")

from functools import reduce

wavelength = 2

bs_cord = (0,0)
bs_ant_num = 8
bs_ant_interval = 1
bs_ant = 1j * np.linspace(bs_cord[1] - bs_ant_interval * (bs_ant_num - 1) / 2,
        bs_cord[1] + bs_ant_interval * (bs_ant_num - 1) / 2, bs_ant_num) + bs_cord[0]

def getMag(xx, yy, bs_ant, weight):
    zz = xx + 1j * yy
    zz = np.absolute(reduce(lambda x, y:x + y, map(lambda x, y:np.exp(-1j * 2 *
        np.pi * np.absolute(zz - x) / wavelength) * np.absolute(np.cos(np.angle(zz - x))) * y, bs_ant, weight)))
    return zz

def type1Pmi(i11, i12,i2, N1, N2, O1, O2):
    Pcsirs = 2 * N1 * N2
    l = i11
    m = i12
    n = i2
    um = np.exp(1j * 2 * np.pi * m / O2 / N2 * np.array(range(0,N2)))
    vl = np.exp(1j * 2 * np.pi * l / O1 / N1 * np.array(range(0,N1)))
    vlm = np.kron(vl, um)
    phin = np.exp(1j * np.pi * n / 2)
    return 1 / np.sqrt(Pcsirs) * (np.append(vlm , [phin * vlm]))

x = np.linspace(0,1000,200)
y = np.linspace(-500,500,200)
N1 = 8
N2 = 1
O1 = 1
O2 = 1
xx, yy = np.meshgrid(x, y)
index = 1
fig = plt.figure()
fig.set_figwidth(N2 * O2 * 5)
fig.set_figheight(N1 * O1 * 5)
for i11 in range(0, N1 * O1):
    for i12 in range(0, N2 * O2):
        pmiWeight = type1Pmi(i11,i12, 1 ,N1, N2 ,O1, O2)
        pmiWeight = pmiWeight[0:N1 * N2]
        print(pmiWeight)
        plt.subplot(N1 * O1, N2 * O2, index)
        zz = getMag(xx, yy, bs_ant,pmiWeight)
        plt.contourf(xx, yy, zz, levels = 50)
        plt.colorbar()
        index += 1
plt.savefig("/mnt/d/pmi.png")
